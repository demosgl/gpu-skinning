#version 150

in vec3 vertex;
in vec3 normal;
in vec2 texcoords;
in vec2 lightcoords;
in int bone;

uniform mat4 u_projection;
uniform mat4 u_view;
layout(std140) uniform BonesBlock
{
   mat4 u_bones[32];
};

out vec2 f_uv_tex;

void main()
{
    mat4 m = u_projection * u_view;
    if (bone >= 0)
    {
        m = m * u_bones[bone];
    }
    gl_Position = m * vec4(vertex.xyz, 1.0);
    f_uv_tex = texcoords;
}
