#include "gpu-skinning.h"
#include <GL/glextl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

bool GpuSkinningProgram::Initialize()
{
    this->meshData = GpuSkinningProgram::LoadMeshData("data/gpu-skinning.mdl");

    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

    this->SetupShaders();

    this->SetupBuffers();

    return true;
}

void GpuSkinningProgram::Render(float time, int width, int height)
{
    auto projection = glm::perspective(90.0f, float(width) / float(height), 0.1f, 4000.0f);
    auto view = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-150.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    glViewport(0, 0, width, height);

    glUseProgram(this->shader);
    glUniformMatrix4fv(this->projection_matrix, 1, false, glm::value_ptr(projection));
    glUniformMatrix4fv(this->view_matrix, 1, false, glm::value_ptr(view));

    glBindBuffer(GL_UNIFORM_BUFFER, this->bone_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, this->meshData.bones.size() * sizeof(glm::mat4), glm::value_ptr(this->meshData.bones[0]));
    glBindBufferRange(GL_UNIFORM_BUFFER, this->bone_buffer_location, this->bone_buffer, 0, this->meshData.bones.size() * sizeof(glm::mat4));

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glBindVertexArray(this->vao);
    for (auto itr = this->meshData.meshes.begin(); itr != this->meshData.meshes.end(); ++itr)
        glDrawArrays(GL_TRIANGLES, itr->firstVertex, itr->vertexCount);
    glBindVertexArray(0);
}

void GpuSkinningProgram::Destroy()
{
    glBindVertexArray(this->vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
    glDeleteBuffers(1, &this->vbo);
    glBindVertexArray(0);
}

int main()
{
    return GpuSkinningProgram().Run();
}
