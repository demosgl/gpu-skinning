#ifndef GPU_SKINNING_PROGRAM_H
#define GPU_SKINNING_PROGRAM_H

#include <program.h>
#include <GL/glextl.h>
#include <glm/glm.hpp>
#include <vector>

typedef struct sVertex
{
    glm::vec3 position;
    glm::vec2 texcoords;
    glm::vec3 normal;
    int bone;

} tVertex;

typedef struct sMesh
{
    int firstVertex;
    int vertexCount;
    unsigned int texture;

} tMesh;

typedef struct sModel
{
    std::vector<tVertex> vertices;
    std::vector<tMesh> meshes;
    std::vector<glm::mat4> bones;

} tModel;

namespace ShaderAttributeLocations
{
    enum {
        Vertex = 0,
        TexCoord,
        Normal,
        Bone
    };
}

class GpuSkinningProgram : public Program
{
public:
    GpuSkinningProgram() : Program("GPU Skinning - demos.gl") { }

    virtual bool Initialize();
    virtual void Render(float time, int width, int height);
    virtual void Destroy();

    virtual void SetupShaders();
    virtual void SetupBuffers();
private:
    GLuint shader;
    GLint projection_matrix, view_matrix;
    GLuint bone_buffer, bone_buffer_location;
    GLuint texture_location;
    GLuint vbo;
    GLuint vao;

    static tModel LoadMeshData(const std::string& filename);
    void CheckShaderStatus(GLuint shader);
    void CheckProgramStatus(GLuint program);

    static glm::vec3 vertices[];

    tModel meshData;

};

#endif  // GPU_SKINNING_PROGRAM_H
