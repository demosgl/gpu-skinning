#include "gpu-skinning.h"
#include <GL/glextl.h>
#include <file-io.h>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

#define MAX_MDL_BONES  128

void GpuSkinningProgram::SetupShaders()
{
    auto vertexShader = FileIO::LoadFileData("data/default-shader.vert");
    auto fragmentShader = FileIO::LoadFileData("data/default-shader.frag");
    const char* v = reinterpret_cast<const char*>(vertexShader.data);
    const char* f = reinterpret_cast<const char*>(fragmentShader.data);

    // Compile and check vertex shader
    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, (const GLchar*const*)&v, NULL);
    glCompileShader(vertShader);
    CheckShaderStatus(vertShader);

    // Compile and check fragment shader
    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, (const GLchar*const*)&f, NULL);
    glCompileShader(fragShader);
    CheckShaderStatus(fragShader);

    // Link and check program linking
    this->shader = glCreateProgram();
    glAttachShader(this->shader, vertShader);
    glAttachShader(this->shader, fragShader);

    // The glue between shaders and buffers.
    // Its important that these attribute locations are bound just before the shader is linked
    glBindAttribLocation(this->shader, ShaderAttributeLocations::Vertex, "vertex");
    glBindAttribLocation(this->shader, ShaderAttributeLocations::Normal, "normal");
    glBindAttribLocation(this->shader, ShaderAttributeLocations::TexCoord, "texcoords");
    glBindAttribLocation(this->shader, ShaderAttributeLocations::Bone, "bone");

    glLinkProgram(this->shader);
    CheckProgramStatus(this->shader);

    // Cleanup after
    glDeleteShader(vertShader);
    glDeleteShader(fragShader);

    glUseProgram(this->shader);

    // Get the locations of the matrix uniforms
    this->projection_matrix = glGetUniformLocation(this->shader, "u_projection");
    this->view_matrix = glGetUniformLocation(this->shader, "u_view");

    // Get the locations of the texture uniforms
    this->texture_location = glGetUniformLocation(this->shader, "u_tex");
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(this->texture_location, 0);

    // Get the locations of the bone buffer uniforms
    this->bone_buffer_location = 0;
    GLint uniform_block_index = glGetUniformBlockIndex(this->shader, "u_bones");
    glUniformBlockBinding(this->shader, uniform_block_index, this->bone_buffer_location);

    // Setup the bone buffer
    glGenBuffers(1, &this->bone_buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, this->bone_buffer);
    glBufferData(GL_UNIFORM_BUFFER, MAX_MDL_BONES * sizeof(glm::mat4), 0, GL_STREAM_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void GpuSkinningProgram::CheckShaderStatus(GLuint shader)
{
    GLint result = GL_FALSE;
    int logLength;

    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        GLchar* error = new GLchar[(logLength > 1) ? logLength : 1];
        glGetShaderInfoLog(shader, logLength, NULL, &error[0]);
        std::cerr << &error[0] << std::endl;
        delete []error;
    }
    else
        std::cout << "Shader compiled" << std::endl;
}

void GpuSkinningProgram::CheckProgramStatus(GLuint program)
{
    GLint result = GL_FALSE;
    int logLength;

    glGetProgramiv(program, GL_LINK_STATUS, &result);
    if (result == GL_FALSE)
    {
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
        GLchar* error = new GLchar[(logLength > 1) ? logLength : 1];
        glGetProgramInfoLog(program, logLength, NULL, &error[0]);
        std::cerr << &error[0] << std::endl;
        delete []error;
    }
    else
        std::cout << "Program linked" << std::endl;
}
