#include "gpu-skinning.h"
#include "hl1mdltypes.h"
#include <file-io.h>
#include <iostream>

glm::vec3 GpuSkinningProgram::vertices[] = {
    glm::vec3(  100.0f, -100.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f),
    glm::vec3(    0.0f,  100.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f),
    glm::vec3( -100.0f, -100.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)
};

tModel GpuSkinningProgram::LoadMeshData(const std::string& filename)
{
    tModel result;
    Array<hl1mdl::tMDLBodyParts> bodyPartData;

    auto fileData = FileIO::LoadFileData(filename);
    auto header = reinterpret_cast<hl1mdl::tMDLHeader*>(fileData.data);

    bodyPartData.Map(header->numbodyparts, reinterpret_cast<hl1mdl::tMDLBodyParts*>(fileData.data + header->bodypartindex));

    for (int i = 0; i < header->numbones; ++i)
    {
        result.bones.push_back(glm::mat4(1.0f));
    }

    for (int i = 0; i < header->numbodyparts; i++)
    {
        hl1mdl::tMDLBodyParts& part = bodyPartData[i];

        Array<hl1mdl::tMDLModel> models(part.nummodels, reinterpret_cast<hl1mdl::tMDLModel*>(fileData.data + part.modelindex));
        for (int j = 0; j < models.count; j++)
        {
            hl1mdl::tMDLModel& model = models[j];

            Array<glm::vec3> vertices(model.numverts, reinterpret_cast<glm::vec3*>(fileData.data + model.vertindex));
            Array<byte> vertexBones(model.numverts, fileData.data + model.vertinfoindex);
            Array<glm::vec3> normals(model.numnorms, reinterpret_cast<glm::vec3*>(fileData.data + model.normindex));

            Array<hl1mdl::tMDLMesh> meshes(model.nummesh, reinterpret_cast<hl1mdl::tMDLMesh*>(fileData.data + model.meshindex));
            for (int k = 0; k < model.nummesh; k++)
            {
                tMesh f;

                f.firstVertex = result.vertices.size();

                short* ptricmds = reinterpret_cast<short*>(fileData.data + meshes[k].triindex);
                while (int vertnum = *(ptricmds++))
                {
                    tVertex first, prev;
                    for(int l = 0; l < abs(vertnum); l++, ptricmds += 4)
                    {
                        tVertex v;

                        v.position = vertices[ptricmds[0]];
                        v.normal = normals[ptricmds[0]];
                        v.bone = int(vertexBones[ptricmds[0]]);

                        if (vertnum < 0)    // TRIANGLE_FAN
                        {
                            if (l == 0)
                                first = v;
                            else if (l == 1)
                                prev = v;
                            else
                            {
                                result.vertices.push_back(first);
                                result.vertices.push_back(prev);
                                result.vertices.push_back(v);

                                prev = v;
                            }
                        }
                        else                // TRIANGLE_STRIP
                        {
                            if (l == 0)
                                first = v;
                            else if (l == 1)
                                prev = v;
                            else
                            {
                                if (l & 1)
                                {
                                    result.vertices.push_back(first);
                                    result.vertices.push_back(v);
                                    result.vertices.push_back(prev);
                                }
                                else
                                {
                                    result.vertices.push_back(first);
                                    result.vertices.push_back(prev);
                                    result.vertices.push_back(v);
                                }

                                first = prev;
                                prev = v;
                            }
                        }
                    }
                }
                f.vertexCount = result.vertices.size() - f.firstVertex;
                result.meshes.push_back(f);
            }
        }
    }
    return result;
}
