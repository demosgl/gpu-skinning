#include "gpu-skinning.h"
#include <GL/glextl.h>

void GpuSkinningProgram::SetupBuffers()
{
    glGenVertexArrays(1, &this->vao);
    glBindVertexArray(this->vao);

    glGenBuffers(1, &this->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

    glBufferData(GL_ARRAY_BUFFER, this->meshData.vertices.size() * sizeof(tVertex), 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, this->meshData.vertices.size() * sizeof(tVertex), reinterpret_cast<GLvoid*>(&this->meshData.vertices[0]));

    glVertexAttribPointer((GLuint)ShaderAttributeLocations::Vertex, 3, GL_FLOAT, GL_FALSE, sizeof(tVertex), 0);
    glEnableVertexAttribArray((GLuint)ShaderAttributeLocations::Vertex);

    glVertexAttribPointer((GLuint)ShaderAttributeLocations::TexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(tVertex), reinterpret_cast<GLvoid*>(sizeof(float) * 3));
    glEnableVertexAttribArray((GLuint)ShaderAttributeLocations::TexCoord);

    glVertexAttribPointer((GLuint)ShaderAttributeLocations::Normal, 3, GL_FLOAT, GL_FALSE, sizeof(tVertex), reinterpret_cast<GLvoid*>(sizeof(float) * 5));
    glEnableVertexAttribArray((GLuint)ShaderAttributeLocations::Normal);

    glVertexAttribPointer((GLuint)ShaderAttributeLocations::Bone, 1, GL_FLOAT, GL_FALSE, sizeof(tVertex), reinterpret_cast<GLvoid*>(sizeof(float) * 8));
    glEnableVertexAttribArray((GLuint)ShaderAttributeLocations::Bone);

    glBindVertexArray(0);
}
